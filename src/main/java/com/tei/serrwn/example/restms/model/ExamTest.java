package com.tei.serrwn.example.restms.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document
@Getter
@Setter
@AllArgsConstructor
@ToString
public class ExamTest {

    private String id;

    private String studentId;

    private String quizId;

    private double score;

    private List<Question> questions;

   // private int rightAnsweredQuestions;

   // private int totalQuestions;

}
