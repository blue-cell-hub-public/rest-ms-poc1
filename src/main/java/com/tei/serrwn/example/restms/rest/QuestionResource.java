package com.tei.serrwn.example.restms.rest;

import com.tei.serrwn.example.restms.model.Answer;
import com.tei.serrwn.example.restms.model.Question;
import com.tei.serrwn.example.restms.repository.QuestionRepository;
import com.tei.serrwn.example.restms.rest.dto.CreateQuestionDto;
import com.tei.serrwn.example.restms.rest.dto.QuestionDto;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/question")
public class QuestionResource {

    @Autowired
    QuestionRepository repo;

    @Autowired
    MapperFacade  mapper;

    @GetMapping
    public List<QuestionDto> question() {
        return
                 mapper.mapAsList(repo.findAll(), QuestionDto.class);
    }


    @RequestMapping( method = RequestMethod.POST)
    public Question createQuestion(@RequestBody final CreateQuestionDto createQuestionDto) {

        Question question = mapper.map(createQuestionDto, Question.class);
        return repo.save(question);
    }
}
