package com.tei.serrwn.example.restms.rest;

import com.tei.serrwn.example.restms.model.ExamTest;
import com.tei.serrwn.example.restms.service.ExamTestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.Mapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/examtest")
public class ExamTestResource {

    @Autowired
    ExamTestService examTestService;

    @GetMapping
    public List<ExamTest> getExamTest () {

        return examTestService.getExamTestByLesson("Computers");

    }
}
