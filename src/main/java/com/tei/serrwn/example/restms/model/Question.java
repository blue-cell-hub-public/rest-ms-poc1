package com.tei.serrwn.example.restms.model;

import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Question {

    private String id;

    private String body;

    private double points;

    private List<Answer> possibleAnswers;

    private Answer rightAnswer;

    private Answer submittedAnswer;

}
