package com.tei.serrwn.example.restms;

import com.tei.serrwn.example.restms.model.Answer;
import com.tei.serrwn.example.restms.model.ExamTest;
import com.tei.serrwn.example.restms.model.Question;
import com.tei.serrwn.example.restms.repository.ExamTestRepository;
import com.tei.serrwn.example.restms.repository.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

@SpringBootApplication
public class RestmsApplication implements CommandLineRunner {

    @Autowired
    QuestionRepository repo;

    @Autowired
    ExamTestRepository examTestRepository;

	public static void main(String[] args) {
		SpringApplication.run(RestmsApplication.class, args);
	}

	@Override
	public void run(String... strings) throws  Exception {

		Question question = new Question( "1","What is MVC", 5.0 ,
				Arrays.asList( new Answer("A", "Model View Controller"),
						new Answer("B", "Mapping View Concurrensy")),
				new Answer( "A", "Model View Controller"), null );


        repo.save(question);

        question = new Question( "2","What is MVC", 5.0 ,
                Arrays.asList( new Answer("A", "Model View Controller"),
                        new Answer("B", "Mapping View Concurrensy")),
                new Answer( "A", "Model View Controller"), null);

        repo.save(question);

         question.setSubmittedAnswer( question.getPossibleAnswers()
                .stream()
                .filter( q -> q.getId().equals("A") )
                .findFirst().get());

        ExamTest examTest =  new ExamTest( "1", "1", "1", 10.0, Arrays.asList( question) );

        examTestRepository.save(examTest);
	}
}
