package com.tei.serrwn.example.restms.service;

import com.tei.serrwn.example.restms.Validation.ValidatorWrapper;
import com.tei.serrwn.example.restms.model.ExamTest;
import com.tei.serrwn.example.restms.model.Student;
import com.tei.serrwn.example.restms.repository.ExamTestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Service
public class ExamTestService {


    private StudentsService studentsService;

    private ExamTestRepository examTestRepository;

    @Autowired
    private ValidatorWrapper validator ;

    public  ExamTestService(StudentsService studentsService, ExamTestRepository examTestRepository) {

        this.studentsService = studentsService ;
        this.examTestRepository = examTestRepository;
    }

    public List<ExamTest> getExamTestByLesson (String lesson) {

        List<Student> students = studentsService.getStudentsByLesson(lesson).getStudents();

        return examTestRepository.
                findAllByStudentIdIn(students.stream()
                        .map(s -> s.getId()).collect(Collectors.toList()));

    }
}
