package com.tei.serrwn.example.restms.service;

import com.tei.serrwn.example.restms.model.Student;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class StudentsService {


    public StudentsDto getStudentsByLesson(String lesson) {

        RestTemplate   rest = new RestTemplate();

        String  uri = "http://localhost:3000/class/" + lesson ;

       ResponseEntity<StudentsDto> resp = rest.getForEntity(uri,StudentsDto.class);
       return  resp.getBody();
    }
}
