package com.tei.serrwn.example.restms.repository;


import com.tei.serrwn.example.restms.model.Question;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionRepository extends MongoRepository<Question, String> {

}
