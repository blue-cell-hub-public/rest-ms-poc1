package com.tei.serrwn.example.restms.rest.dto;

import com.tei.serrwn.example.restms.model.Answer;
import lombok.Value;

import java.util.List;

@Value
public class QuestionDto {

    private String id;

    private String body;

    private double points;

    private List<Answer> possibleAnswers;

    private Answer submittedAnswer;
}
