package com.tei.serrwn.example.restms.service;

import com.tei.serrwn.example.restms.model.Question;
import com.tei.serrwn.example.restms.repository.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuestionService {



    @Autowired
    QuestionRepository questionRepository;

    public List<Question> getAllQuestions() {

        return questionRepository.findAll();
    }

}
