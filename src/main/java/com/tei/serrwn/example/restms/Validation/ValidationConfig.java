package com.tei.serrwn.example.restms.Validation;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

@Configuration
public class ValidationConfig {

    @Bean(name = "customValidatorFactory")
    public LocalValidatorFactoryBean validatorFactoryBean() {
        return new LocalValidatorFactoryBean();
    }

    @Bean
    public  ValidatorWrapper validatorWrapper() {
        return  new ValidatorWrapper();
    }
}
