package com.tei.serrwn.example.restms.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document
public class Quiz {

    @Id
    private String id;

    private String name;

    private String lesson;

    @DBRef
    private List<Question> questions;


}