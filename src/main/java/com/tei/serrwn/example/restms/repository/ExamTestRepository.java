package com.tei.serrwn.example.restms.repository;

import com.tei.serrwn.example.restms.model.ExamTest;
import com.tei.serrwn.example.restms.model.Student;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public interface ExamTestRepository extends MongoRepository<ExamTest, String> {

    List<ExamTest> findAllByStudentIdIn(List<String> students);
}
