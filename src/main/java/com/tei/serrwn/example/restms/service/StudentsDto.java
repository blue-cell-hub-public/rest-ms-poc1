package com.tei.serrwn.example.restms.service;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.tei.serrwn.example.restms.model.Student;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(  ignoreUnknown = true)
@Getter
@Setter
@AllArgsConstructor
@ToString
public class StudentsDto {

    private String id;
    private List<Student> students;
}
