package com.tei.serrwn.example.restms.rest.dto;

import com.tei.serrwn.example.restms.model.Answer;
import lombok.Data;

import java.util.List;

@Data
public class CreateQuestionDto {
    private String id;

    private String body;

    private double points;

    private List<Answer> possibleAnswers;

    private Answer rightAnswer;

}
