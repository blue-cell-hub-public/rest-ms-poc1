package com.tei.serrwn.example.restms.Validation;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.util.Set;


public class ValidatorWrapper {

    @Autowired
    @Qualifier(value = "customValidatorFactory")
    private Validator validator;


    public void validate(final Object model) {
        final Set<ConstraintViolation<Object>> violations = validator.validate(model);

        if (!violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
    }
}
