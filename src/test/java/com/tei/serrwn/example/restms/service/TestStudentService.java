package com.tei.serrwn.example.restms.service;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestStudentService {


    @Test
    public void getStudentsByLessonsTest () {

        StudentsService  studentsService = new StudentsService();

        studentsService.getStudentsByLesson("Computers")
                .getStudents().stream().forEach(System.out::println);


    }
}
