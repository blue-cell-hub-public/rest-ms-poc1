package com.tei.serrwn.example.restms.service;


import com.tei.serrwn.example.restms.model.ExamTest;
import com.tei.serrwn.example.restms.model.Student;
import com.tei.serrwn.example.restms.repository.ExamTestRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.mockito.Mockito.*;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
        TestExamTestService.TestConfiguration.class
})
public class TestExamTestService {

    @Configuration
    public static class TestConfiguration {


        @Bean
        public StudentsService studentsService() {

            return mock(StudentsService.class);
        }

        @Bean
        public ExamTestService examTestService() {

            return  new ExamTestService( studentsService(), examTestRepository() );
        }



        @Bean
        public ExamTestRepository examTestRepository() {
            return  mock(ExamTestRepository.class);
        }


    }

    @Autowired
    StudentsService studentsService;

    @Autowired
    ExamTestRepository examTestRepository;

    @Autowired
    ExamTestService examTestService;


    @Before
    public void setup() {
        reset(studentsService);
    }

    @Test
    public void testGetExamTestByLesson() {

        StudentsDto dto = new StudentsDto( "Computers",
                Arrays.asList(
                        new Student("1", "John Balanos"),
                        new Student("2", "Nikos Petalidis")));

        when(studentsService.getStudentsByLesson("Computers")).thenReturn(dto);

        ExamTest examTest1 = new ExamTest("1","1", "1", 10.0, null);
        ExamTest examTest2 = new ExamTest("1","2", "1", 10.0, null);

        when(examTestRepository.findAllByStudentIdIn(dto.getStudents().stream().
                map(s -> s.getId()).collect(Collectors.toList()))).thenReturn(Arrays.asList( examTest1,examTest2));


        List<ExamTest> examTestList = examTestService.getExamTestByLesson("Computers");

    }
}
